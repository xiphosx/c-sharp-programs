using System;

namespace PasswordChecker {
  class Program {
    public static void Main (string[] args) {
      //This is a C# Password Checker program that will ask the user to input a password, and using conditional logic and control flow, it will rate the password.
      
      // Defining the standards for a password
      int minLength = 10;
      string uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      string lowercase = "abcdefghijklmnopqrstuvwxyz";
      string digits = "0123456789";
      string specialChars = "!@#$%^&*";

      // Ask the user to enter a password and save input
      Console.WriteLine ("Enter a password: ");
      string userPassword = Console.ReadLine ();

      // Variable to hold security score
      int score = 0;

      // Checks if password is greater than or equal to minimum length
      if (userPassword.Length >= minLength) {
        score++;
      }

      // Checks if password contains uppercase letters
      if (Tools.Contains (userPassword, uppercase)) {
        score++;
      }

      // Checks if password contains lowercase letters
      if (Tools.Contains (userPassword, lowercase)) {
        score++;
      }

      // Checks if password contains digits
      if (Tools.Contains (userPassword, digits)) {
        score++;
      }

      // Checks if password contains special characters
      if (Tools.Contains (userPassword, specialChars)) {
        score++;
      }

      // Checks if password is ridiculous
      if (userPassword == "password" || userPassword == "1234") {
        score = 0;
      }

      // Print out security score
      Console.WriteLine ($"Password score: {score}");

      // Let user know how they did
      switch (score) {
        case 5:
          Console.WriteLine ("Your password is unbreakable.");
          break;
        case 4:
          Console.WriteLine ("Your password is extremely strong.");
          break;
        case 3:
          Console.WriteLine ("Your password is strong.");
          break;
        case 2:
          Console.WriteLine ("Your password is medium.");
          break;
        case 1:
          Console.WriteLine ("Your password is weak.");
          break;
        case 0:
          Console.WriteLine ("Are you serious?");
          break;
        default:
          Console.WriteLine ("Your password doesn't meet any of the standards.");
          break;
      }
    }
  }
}