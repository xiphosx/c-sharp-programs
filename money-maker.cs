namespace MoneyMaker
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Money Maker!");

            //This is a C# Money Maker program I wrote for Codecademy using arithmetic variables and methods to caculate the minimum number of coins that would equal a user's input value.
                        
            // Ask user for the amount to convert and capture in a variable
            Console.Write("Enter an amount to convert to coins: ");
            string amount = Console.ReadLine();
    
            // Capture the string value to a number
            double amountConverted = Convert.ToDouble(amount);
                    
            amountConverted = Math.Floor(amountConverted);
                      
            // Let user know what you are about to do
            Console.WriteLine($"{amountConverted} cents is equal to...");
                        
            // Define coin values
            int goldValue = 10;
            int silverValue = 15;
                        
            double goldCoins = Math.Floor(amountConverted / goldValue);
            double remainder = amountConverted / goldValue;
                        
            double silverCoins = Math.Floor(remainder / silverValue);
            remainder = Math.Floor(remainder % silverValue);
                        
            Console.WriteLine($"Gold coins: {goldCoins}");
            Console.WriteLine($"Silver coins: {silverCoins}");
            Console.WriteLine($"Bronze coins: {remainder}");
        }
    }
}                  