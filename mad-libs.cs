using System;

namespace Madlibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /* This program will use string interpolation to allow us to transform a piece of text by swapping out different pieces of information.
            
           Author: Molinet
           */

            // Let the user know that the program is starting:
            Console.WriteLine("Welcome. The Mad Libs game is about to begin.");

            // Give the Mad Lib a title:
            string title = "The Fox and the Wolf";

            Console.WriteLine(title);
            // Define user input and variables:
            Console.Write("Enter a name: ");
            string name = Console.ReadLine();

            Console.Write("You'll need to pick three adjectives next. What would you like your first one to be? ");
            string adjectiveOne = Console.ReadLine();

            Console.Write("Choose a second adjective: ");
            string adjectiveTwo = Console.ReadLine();

            Console.Write("And choose the third adjective: ");
            string adjectiveThree = Console.ReadLine();

            Console.Write("Next up is choosing one verb: ");
            string verb = Console.ReadLine();

            Console.Write("You now need to choose two nouns. What would you like the first one to be? ");
            string nounOne = Console.ReadLine();

            Console.Write("And the second noun? ");
            string nounTwo = Console.ReadLine();

            Console.Write("And lastly, I'm going to ask you to choose some fun and weird choices. \nThe first one is to pick an animal: ");
            string animal = Console.ReadLine();

            Console.Write("Pick a food: ");
            string food = Console.ReadLine();

            Console.Write("Pick a fruit: ");
            string fruit = Console.ReadLine();

            Console.Write("Pick a superhero: ");
            string superhero = Console.ReadLine();

            Console.Write("Pick a country: ");
            string country = Console.ReadLine();

            Console.Write("Pick a dessert: ");
            string dessert = Console.ReadLine();

            Console.Write("And lastly, pick a year: ");
            string year = Console.ReadLine();

            // The template for the story:
            string story = $"This morning {name} woke up feeling {adjectiveOne}. 'It is going to be a {adjectiveTwo} day!' Outside, a bunch of {animal}s were protesting to keep {food} in stores. They began to {verb} to the rhythm of the {nounOne}, which made all the {fruit}s very {adjectiveThree}. Concerned, {name} texted {superhero}, who flew {name} to {country} and dropped {name} in a puddle of frozen {dessert}. {name} woke up in the year {year}, in a world where {nounTwo}s ruled the world.";

            // Print the story:
            Console.WriteLine(story);
        }
    }
}